package mn.telegram

import groovy.transform.ToString

@ToString
class Message {
    String chat_id
    String text
    String parse_mode = 'markdown'
    Location location
}
