package mn.telegram

import groovy.transform.ToString

@ToString
class Response<T> {

    boolean ok;

    T result;

}
