package mn.telegram

import io.micronaut.http.annotation.Body
import io.micronaut.http.client.multipart.MultipartBody
import io.reactivex.Single


interface TelegramClient {

    Single<Message> sendMessage( @Body Message message)

    Single<Message> sendPhoto( @Body MultipartBody photo)

    Single<Message> sendAnimation( @Body MultipartBody animation)

}