package mn.telegram

import groovy.util.logging.Log

@Log
class TelegramApi {

    TelegramClient telegramClient

    TelegramApi(TelegramClient telegramClient){
        this.telegramClient = telegramClient
    }

    void sendMessage(Message message){
        log.info "sendMessage to $message.chat_id "
        telegramClient.sendMessage(message).subscribe()
    }

    void sendPhoto(SendPhoto sendPhoto) {
        log.info "sendPhoto to $sendPhoto.chat_id "
        telegramClient.sendPhoto(sendPhoto.multipartBody()).subscribe()
    }

    void sendAnimation(SendAnimation sendAnimation) {
        log.info "sendAnimation to $sendAnimation.chat_id "
        telegramClient.sendAnimation(sendAnimation.multipartBody()).subscribe()
    }

}
