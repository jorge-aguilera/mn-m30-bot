package mn.telegram

import groovy.transform.ToString

@ToString(includeNames = true)
class Location {

    float longitude
    float latitude

}
