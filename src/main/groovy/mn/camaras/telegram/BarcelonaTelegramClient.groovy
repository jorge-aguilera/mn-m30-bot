package mn.camaras.telegram

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.multipart.MultipartBody
import io.reactivex.Single
import mn.telegram.Message
import mn.telegram.TelegramClient

@Client('https://api.telegram.org')
interface BarcelonaTelegramClient extends TelegramClient{

    @Post('/bot${telegram.tokens.barcelona}/sendMessage')
    Single<Message> sendMessage(@Body Message message)

    @Post(value='/bot${telegram.tokens.barcelona}/sendPhoto', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendPhoto( @Body MultipartBody photo)

    @Post(value='/bot${telegram.tokens.barcelona}/sendAnimation', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendAnimation( @Body MultipartBody animation)

}