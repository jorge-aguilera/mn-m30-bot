package mn.camaras.telegram

import mn.AppConfig
import mn.telegram.TelegramApi
import mn.telegram.TelegramClient

import javax.inject.Singleton

@Singleton
class TelegramApiFactory {

    AppConfig appConfig

    MadridTelegramClient madridTelegramClient

    BarcelonaTelegramClient barcelonaTelegramClient

    GranadaTelegramClient granadaTelegramClient

    SevillaTelegramClient sevillaTelegramClient

    ValladolidTelegramClient valladolidTelegramClient

    TelegramApiFactory(AppConfig appConfig,
                       MadridTelegramClient madridTelegramClient,
                       BarcelonaTelegramClient barcelonaTelegramClient,
                       GranadaTelegramClient granadaTelegramClient,
                       SevillaTelegramClient sevillaTelegramClient,
                       ValladolidTelegramClient valladolidTelegramClient) {
        this.appConfig = appConfig
        this.madridTelegramClient = madridTelegramClient
        this.barcelonaTelegramClient = barcelonaTelegramClient
        this.granadaTelegramClient = granadaTelegramClient
        this.sevillaTelegramClient = sevillaTelegramClient
        this.valladolidTelegramClient = valladolidTelegramClient
    }

    TelegramApi newTelegramApi(String id){
        TelegramClient telegramClient
        switch (id){
            case 'Madrid':
                telegramClient = madridTelegramClient
                break
            case 'Barcelona':
                telegramClient = barcelonaTelegramClient
                break
            case 'Granada':
                telegramClient = granadaTelegramClient
                break
            case 'Sevilla':
                telegramClient = sevillaTelegramClient
                break
            case 'Valladolid':
                telegramClient = valladolidTelegramClient
                break
            default:
                throw new RuntimeException("$id not reconigsed")
        }
        new TelegramApi(telegramClient)
    }


}
