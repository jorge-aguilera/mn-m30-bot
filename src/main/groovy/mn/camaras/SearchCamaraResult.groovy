package mn.camaras

class SearchCamaraResult {
    String search
    String id
    String name
    String image
    List<Camara> alternates
}
