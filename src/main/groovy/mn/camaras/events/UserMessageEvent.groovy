package mn.camaras.events


import groovy.transform.ToString

@ToString
class UserMessageEvent{

    String city

    String chat_id
    String username
    String text

}
