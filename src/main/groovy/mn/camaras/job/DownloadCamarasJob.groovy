package mn.camaras.job

import io.micronaut.scheduling.annotation.Scheduled
import javax.inject.Singleton

@Singleton
class DownloadCamarasJob {

    DownloadCamaras downloadCamaras

    DownloadCamarasJob(DownloadCamaras downloadCamaras){
        this.downloadCamaras = downloadCamaras
    }

    @Scheduled(fixedDelay = '${download.every}', initialDelay = "5s")
    void executeDownloadMadrid() {
        downloadCamaras.downloadMadrid()
    }

    @Scheduled(fixedDelay = '${download.every}', initialDelay = "25s")
    void executeCronDownloadBarcelona() {
        downloadCamaras.downloadBarcelona()
    }

    @Scheduled(fixedDelay = '${download.every}', initialDelay = "35s")
    void executeCronDownloadGranada() {
        downloadCamaras.downloadGranada()
    }

    @Scheduled(fixedDelay = '${download.every}', initialDelay = "45s")
    void executeCronDownloadSevilla() {
        downloadCamaras.downloadSevilla()
    }

    @Scheduled(fixedDelay = '${download.every}', initialDelay = "55s")
    void executeCronDownloadValladolid() {
        downloadCamaras.downloadValladolid()
    }
}
