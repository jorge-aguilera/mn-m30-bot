package mn.camaras.job

import io.micronaut.context.annotation.Value
import mn.AppConfig
import mn.camaras.Camara
import mn.camaras.services.BarcelonaService
import mn.camaras.services.GranadaService
import mn.camaras.services.MadridService
import mn.camaras.services.SevillaService
import mn.camaras.services.ValladolidService

import javax.annotation.PostConstruct
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DownloadCamaras {

    @Inject
    protected AppConfig appConfig

    @Value('${download.path}')
    protected String path

    @Value('${download.max:5}')
    protected int maxFiles

    protected File root

    protected MadridService madridService = new MadridService()
    protected BarcelonaService barcelonaService = new BarcelonaService()
    protected GranadaService granadaService = new GranadaService()
    protected SevillaService sevillaService = new SevillaService()
    protected ValladolidService valladolidService = new ValladolidService()

    @PostConstruct
    void init(){
        root = new File(path)
        root.mkdirs()
        madridService.init(appConfig)
        barcelonaService.init(appConfig)
        granadaService.init(appConfig)
        sevillaService.init(appConfig)
        valladolidService.init(appConfig)
    }

    List<File> listCityFiles(String ciudad, String id){
        File tmp = new File(root,ciudad)
        File dir = new File(tmp, id)
        dir.listFiles().sort{ it.lastModified() } as List<File>
    }

    void downloadMadrid() {
        downloadBatch( madridService.camaras(), 'madrid')
    }

    void downloadBarcelona() {
        downloadBatch( barcelonaService.camaras(), 'barcelona')
    }

    void downloadGranada() {
        downloadBatch( granadaService.camaras(), 'granada')
    }

    void downloadSevilla() {
        downloadBatch( sevillaService.camaras(), 'sevilla')
    }

    void downloadValladolid() {
        downloadBatch( valladolidService.camaras(), 'valladolid')
    }


    void downloadBatch( List<Camara> camaras, String ciudad){
        File tmp = new File(root,ciudad)
        tmp.mkdirs()
        camaras.each{ Camara c ->
            try {
                File d = new File(tmp, "${c.id}")
                d.mkdirs()
                byte[] current = c.url.toURL().bytes

                List<File> files = d.listFiles().sort { it.lastModified() } as List<File>
                if (files.size() == 0) {
                    new File(d, new Date().time + ".jpg").bytes = current
                    return
                }
                File fLast = files.last()
                if (current.md5() != fLast.bytes.md5()) {
                    new File(d, new Date().time + ".jpg").bytes = current
                    if (files.size() >= maxFiles) {
                        files.first().delete()
                    }
                }
            }catch(e){
                println "$ciudad $c.nombre error $e.message"
            }
        }

    }


}
