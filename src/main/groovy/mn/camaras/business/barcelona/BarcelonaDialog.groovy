package mn.camaras.business.barcelona

import mn.camaras.business.SearchDialogTrait
import mn.camaras.services.BarcelonaService


class BarcelonaDialog implements SearchDialogTrait{

    @Override
    String getCiudad() {
        'barcelona'
    }

    BarcelonaService searchCamaraService

    void executeCommand() {
        switch(command.toLowerCase()){
            case '/dalt':
                command="29,31,32,41"
                buildSearch('Ronda de Dalt')
            break
            case '/litoral':
                command="30,39,40,47"
                buildSearch('Ronda Litoral')
                break
            default:
                buildSorry()
                break
        }
    }

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Barcelona
Estos son los comandos disponibles actualmente

/start  (este mensaje)
/camaras  (para ver un listado de todas las camaras)
/dalt    Ronda de Dalt
/litoral Ronda Litoral 

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""
}
