package mn.camaras.business.barcelona

import groovy.util.logging.Log
import mn.camaras.business.BaseTelegramDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.services.BarcelonaService
import mn.camaras.services.BarilocheService


@Log
class BarcelonaTelegramDialog extends BaseTelegramDialog{

    void onUserMessageEvent(UserMessageEvent userMessageEvent) {

        if (userMessageEvent.city != 'Barcelona')
            return

        BarcelonaService barcelonaService = new BarcelonaService(appConfig: appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        new BarcelonaDialog(
                barilocheService: barilocheService,
                searchCamaraService: barcelonaService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
                downloadCamaras: downloadCamaras
        ).execute()

    }

}
