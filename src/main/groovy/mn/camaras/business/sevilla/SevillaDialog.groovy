package mn.camaras.business.sevilla

import mn.camaras.business.SearchDialogTrait
import mn.camaras.services.BarcelonaService
import mn.camaras.services.SevillaService

class SevillaDialog implements SearchDialogTrait{

    @Override
    String getCiudad() {
        'sevilla'
    }

    SevillaService searchCamaraService

    void executeCommand() {
        switch(command.toLowerCase()){
            case '/eje':
                command="45,46,30,6,40,21,7"
                buildSearch('Eje Norte-Sur')
                break
            case '/rio':
                command="6,5,52,2,9,34,17,8,19,26"
                buildSearch('Rio')
                break
            default:
                buildSorry()
                break
        }
    }

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Sevilla
Estos son los comandos disponibles actualmente

/start  (este mensaje)
/camaras  (para ver un listado de todas las camaras)

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""
}
