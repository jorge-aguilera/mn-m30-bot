package mn.camaras.business.sevilla

import groovy.util.logging.Log
import mn.camaras.business.BaseTelegramDialog
import mn.camaras.business.barcelona.BarcelonaDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.services.BarcelonaService
import mn.camaras.services.BarilocheService
import mn.camaras.services.SevillaService

@Log
class SevillaTelegramDialog extends BaseTelegramDialog{

    void onUserMessageEvent(UserMessageEvent userMessageEvent) {

        if (userMessageEvent.city != 'Sevilla')
            return

        SevillaService sevillaService = new SevillaService(appConfig: appConfig)

        new SevillaDialog(
                searchCamaraService: sevillaService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
                downloadCamaras: downloadCamaras
        ).execute()

    }

}
