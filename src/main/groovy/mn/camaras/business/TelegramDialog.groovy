package mn.camaras.business

import mn.AppConfig
import mn.camaras.business.barcelona.BarcelonaTelegramDialog
import mn.camaras.business.granada.GranadaTelegramDialog
import mn.camaras.business.madrid.MadridTelegramDialog
import mn.camaras.business.sevilla.SevillaTelegramDialog
import mn.camaras.business.valladolid.ValladolidTelegramDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.job.DownloadCamaras
import mn.camaras.telegram.TelegramApiFactory
import mn.telegram.Message
import mn.telegram.TelegramApi

import javax.inject.Singleton

@Singleton
class TelegramDialog {

    DownloadCamaras downloadCamaras

    AppConfig appConfig

    TelegramApiFactory telegramApiFactory

    TelegramDialog(AppConfig appConfig, TelegramApiFactory telegramApiFactory, DownloadCamaras downloadCamaras){
        this.appConfig = appConfig
        this.telegramApiFactory = telegramApiFactory
        this.downloadCamaras = downloadCamaras
    }

    void onUserMessageEvent(UserMessageEvent  userMessageEvent){
        TelegramApi telegramApi = telegramApiFactory.newTelegramApi(userMessageEvent.city)
        BaseTelegramDialog dialog
        switch (userMessageEvent.city){
            case 'Madrid':
                dialog = new MadridTelegramDialog(appConfig:appConfig, telegramApi: telegramApi, downloadCamaras: downloadCamaras)
                break
            case 'Barcelona':
                dialog = new BarcelonaTelegramDialog(appConfig:appConfig, telegramApi: telegramApi, downloadCamaras: downloadCamaras)
                break
            case 'Granada':
                dialog = new GranadaTelegramDialog(appConfig:appConfig, telegramApi: telegramApi, downloadCamaras: downloadCamaras)
                break
            case 'Sevilla':
                dialog = new SevillaTelegramDialog(appConfig:appConfig, telegramApi: telegramApi, downloadCamaras: downloadCamaras)
                break
            case 'Valladolid':
                dialog = new ValladolidTelegramDialog(appConfig:appConfig, telegramApi: telegramApi, downloadCamaras: downloadCamaras)
                break
            default:
                return
        }
        dialog.onUserMessageEvent(userMessageEvent)
        dialog.telegramApi.sendMessage(new Message(
                chat_id: '563785864',
                text: "$userMessageEvent.username está usando $userMessageEvent.city $userMessageEvent.text"
        ))
    }

}
