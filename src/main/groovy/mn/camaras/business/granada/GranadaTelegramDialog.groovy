package mn.camaras.business.granada

import groovy.util.logging.Log
import mn.camaras.business.BaseTelegramDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.services.BarilocheService
import mn.camaras.services.GranadaService
import mn.camaras.services.SierraNevadaService

@Log
class GranadaTelegramDialog extends BaseTelegramDialog{

    void onUserMessageEvent(UserMessageEvent  userMessageEvent) {

        if (userMessageEvent.city != 'Granada')
            return

        GranadaService granadaService = new GranadaService(appConfig:appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        SierraNevadaService sierraNevadaService = new SierraNevadaService(appConfig: appConfig)

        new GranadaDialog(
                sierraNevadaService:sierraNevadaService,
                barilocheService: barilocheService,
                searchCamaraService: granadaService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
                downloadCamaras: downloadCamaras
        ).execute()

    }

}
