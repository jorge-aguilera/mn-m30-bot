package mn.camaras.business

import mn.AppConfig
import mn.camaras.events.UserMessageEvent
import mn.camaras.job.DownloadCamaras
import mn.telegram.TelegramApi

abstract class BaseTelegramDialog {

    DownloadCamaras downloadCamaras

    AppConfig appConfig

    TelegramApi telegramApi

    abstract void onUserMessageEvent(UserMessageEvent  userMessageEvent)

}
