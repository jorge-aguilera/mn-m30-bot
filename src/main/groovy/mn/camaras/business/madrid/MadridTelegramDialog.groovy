package mn.camaras.business.madrid

import groovy.util.logging.Log
import mn.camaras.business.BaseTelegramDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.services.BarilocheService
import mn.camaras.services.DgtService
import mn.camaras.services.M30Service
import mn.camaras.services.MadridService


@Log
class MadridTelegramDialog extends BaseTelegramDialog{

    void onUserMessageEvent(UserMessageEvent  userMessageEvent){

        if( userMessageEvent.city != 'Madrid')
            return

        M30Service m30Service = new M30Service(appConfig)

        MadridService madridService = new MadridService(appConfig: appConfig)

        BarilocheService barilocheService = new BarilocheService(appConfig: appConfig)

        DgtService dgtService = new DgtService(appConfig: appConfig)

        new MadridDialog(
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                barilocheService: barilocheService,
                searchCamaraService: madridService,
                m30Service: m30Service,
                dgtService: dgtService,
                telegramApi: telegramApi,
                downloadCamaras: downloadCamaras
        ).execute()

    }
}
