package mn.camaras.business.valladolid

import mn.camaras.business.SearchDialogTrait
import mn.camaras.services.SevillaService
import mn.camaras.services.ValladolidService

class ValladolidDialog implements SearchDialogTrait{

    @Override
    String getCiudad() {
        'valladolid'
    }

    ValladolidService searchCamaraService

    void executeCommand() {
        switch(command.toLowerCase()){
            default:
                buildSorry()
                break
        }
    }

    String welcomeMessage = """
Hola. Soy un simple bot que intenta mostrarte el estado de las cámaras de tráfico de Sevilla
Estos son los comandos disponibles actualmente

/start  (este mensaje)
/camaras  (para ver un listado de todas las camaras)

Cualquier otro texto intentaré buscar una calle que se parezca y enviarte su estado

Made with ❤ by PuraVida Software
"""
}
