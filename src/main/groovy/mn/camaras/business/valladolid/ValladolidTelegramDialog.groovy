package mn.camaras.business.valladolid

import groovy.util.logging.Log
import mn.camaras.business.BaseTelegramDialog
import mn.camaras.events.UserMessageEvent
import mn.camaras.services.ValladolidService

@Log
class ValladolidTelegramDialog extends BaseTelegramDialog{

    void onUserMessageEvent(UserMessageEvent userMessageEvent) {

        if (userMessageEvent.city != 'Valladolid')
            return

        ValladolidService valladolidService = new ValladolidService(appConfig: appConfig)

        new ValladolidDialog(
                searchCamaraService: valladolidService,
                chatId: userMessageEvent.chat_id,
                command: userMessageEvent.text,
                telegramApi: telegramApi,
                downloadCamaras: downloadCamaras
        ).execute()

    }

}
