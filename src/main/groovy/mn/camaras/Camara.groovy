package mn.camaras

class Camara {
    int id
    String ciudad
    String nombre
    String url

    @Override
    String toString() {
        "($id) $nombre"
    }
}
