package mn.camaras

import groovy.util.logging.Log

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces
import io.reactivex.Single
import mn.telegram.Chat
import mn.telegram.From
import mn.telegram.Message
import mn.telegram.SendAnimation
import mn.telegram.SendPhoto
import mn.telegram.TelegramApi
import mn.camaras.telegram.TelegramApiFactory
import mn.telegram.Update
import mn.telegram.UserMessage

import javax.inject.Inject

@Log
@Controller("/esteeselpuntodeentradaalbot")
public class BotController {

    @Inject
    TelegramApiFactory telegramApiFactory

    @Post("/madrid")
    @Produces(MediaType.TEXT_PLAIN)
    String madrid(Update update) {
        notifyEvent('Madrid', update)
        "OK"
    }

    @Post("/barcelona")
    @Produces(MediaType.TEXT_PLAIN)
    String bacerlona(Update update) {
        notifyEvent('Barcelona', update)
        "OK"
    }

    @Post("/granada")
    @Produces(MediaType.TEXT_PLAIN)
    String granada(Update update) {
        notifyEvent('Granada', update)
        "OK"
    }

    @Post("/sevilla")
    @Produces(MediaType.TEXT_PLAIN)
    String sevilla(Update update) {
        notifyEvent('Sevilla', update)
        "OK"
    }

    @Post("/valladolid")
    @Produces(MediaType.TEXT_PLAIN)
    String valladolid(Update update) {
        notifyEvent('Valladolid', update)
        "OK"
    }

    @Inject
    mn.camaras.business.TelegramDialog telegramDialog

    void notifyEvent(String city, Update update) {
        Single.create { emitter ->
            log.info update.toString()
            try {
                mn.camaras.events.UserMessageEvent userMessageEvent = new mn.camaras.events.UserMessageEvent(
                        city: city,
                        chat_id: update.message.chat.id,
                        username: update.message.chat.username,
                        text: update.message.text,
                )

                telegramDialog.onUserMessageEvent(userMessageEvent)

                log.info "notyfied $update"
            } catch (Exception e) {
                e.printStackTrace()
                log.severe("$e")
                try {
                    mn.camaras.events.UserMessageEvent sorry = new mn.camaras.events.UserMessageEvent(
                            city: city,
                            chat_id: update.message.chat.id,
                            username: update.message.chat.username,
                            text: "uffff, he tenido algún error y me es imposible atender esa petición, sorry",
                    )

                    telegramDialog.onUserMessageEvent(sorry)
                } catch (e2) {
                }
            }
            emitter.onSuccess(Void)
        }.subscribe({

        },{

        })
    }

}
