package mn.camaras.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import groovy.util.slurpersupport.GPathResult
import io.micronaut.core.io.ResourceResolver
import io.micronaut.core.io.scan.ClassPathResourceLoader
import mn.AppConfig
import mn.camaras.Camara

@CompileStatic(TypeCheckingMode.SKIP)
@Log
class MadridService extends SearchCamaraService{

    @Override
    String getCiudad() {
        'madrid'
    }

    void init(AppConfig appConfig){
        GPathResult kml

        //primer byte tiene un caracter desconocido
        ClassPathResourceLoader loader = new ResourceResolver().getLoader(ClassPathResourceLoader.class).get()
        String xml = loader.getResource(appConfig.cctvUrl).get().text
        xml = xml.substring(xml.indexOf('<'))
        println(xml[0..100])

        kml = new XmlSlurper().parseText(xml).declareNamespace("xmlns":"http://earth.google.com/kml/2.2")

        kml.Document.Placemark.sort{
            it.ExtendedData.Data[1].Value.text()
        }.eachWithIndex{ item, idx->
            String description = item.description.text()
            String url = description.split(' ').find{ it.startsWith('src=')}.substring(4)
            Camara c = new Camara( ciudad: 'madrid',
                    id: idx,
                    nombre:item.ExtendedData.Data[1].Value.text(),
                    url:url
            )
            camaras.add c
        }
    }

}
