package mn.camaras.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import mn.AppConfig
import mn.camaras.Camara

@CompileStatic(TypeCheckingMode.SKIP)
@Log
class SevillaService extends SearchCamaraService{

    @Override
    String getCiudad() {
        'sevilla'
    }

    void init(AppConfig appConfig) {
        try {
            def xml = new XmlParser().parse("http://trafico.sevilla.org/camaras.xml".toURL().newInputStream())
            xml.camara.eachWithIndex { item, idx ->
                Camara c = new Camara(ciudad: 'sevilla',
                        id: item["@codigo"] as int,
                        nombre: item["@descripcion"],
                        url: "http://trafico.sevilla.org/camaras/cam${item["@codigo"]}.jpg"
                )
                camaras.add c
            }
        }catch(e){
            e.printStackTrace()
        }
    }
}
