package mn.camaras.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import io.micronaut.core.io.ResourceResolver
import io.micronaut.core.io.scan.ClassPathResourceLoader
import mn.AppConfig
import mn.camaras.Camara
import org.cyberneko.html.parsers.SAXParser

@CompileStatic(TypeCheckingMode.SKIP)
@Log
class GranadaService extends SearchCamaraService{

    @Override
    String getCiudad() {
        'granada'
    }

    void init(AppConfig appConfig) {
        SAXParser parser = new SAXParser()
        parser.setFeature('http://xml.org/sax/features/namespaces',false)

        ClassPathResourceLoader loader = new ResourceResolver().getLoader(ClassPathResourceLoader.class).get()
        String text = new InputStreamReader(loader.getResource(appConfig.granadaUrl).get().openStream(),'UTF-8').text
        def html = new XmlSlurper(parser).parseText( text )

        html."**".findAll { it."@class"=='alaizquierda camara'}.eachWithIndex { item, idx ->

            Camara c = new Camara(ciudad: 'granada',
                    id: idx,
                    nombre: item.P.IMG."@alt".text(),
                    url:  'http://www.movilidadgranada.com'+item.P.IMG."@src".text()
            )
            camaras.add c
        }
    }
}
