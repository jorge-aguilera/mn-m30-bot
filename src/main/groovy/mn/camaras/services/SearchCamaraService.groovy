package mn.camaras.services

import groovy.transform.Synchronized
import groovy.util.logging.Log
import mn.AppConfig
import mn.camaras.Camara
import mn.camaras.SearchCamaraResult
import mn.camaras.job.DownloadCamaras

@Log
abstract class SearchCamaraService {

    AppConfig appConfig

    List<Camara> camaras=[]

    abstract void init(AppConfig appConfig)

    abstract String getCiudad()

    @Synchronized
    List<Camara> camaras(){
        if( camaras.size() == 0)
            init(appConfig)
        camaras
    }

    SearchCamaraResult searchCamara(final String search){
        SearchCamaraResult ret = new SearchCamaraResult(search: search)

        final String searchUpperCase = search.toUpperCase()
        boolean byNumber = searchUpperCase.isNumber()
        def places = camaras().findAll { camara ->
            if (byNumber)
                return camara.id == (searchUpperCase as int)
            else
                return camara.nombre.indexOf(searchUpperCase) != -1
        }

        if (places.size()) {
            def first = places.first()
            ret.id = first.id
            ret.name = first.toString()
            log.info "found $ret.name $first.url"
            ret.image = first.url
            ret.alternates = places.drop(1)
        }
        log.info "Search $search found ${ret.alternates} places"

        ret
    }
}
