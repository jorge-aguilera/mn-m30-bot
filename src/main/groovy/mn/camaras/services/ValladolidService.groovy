package mn.camaras.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import mn.AppConfig
import mn.camaras.Camara

@CompileStatic(TypeCheckingMode.SKIP)
@Log
class ValladolidService extends SearchCamaraService{

    @Override
    String getCiudad() {
        'valladolid'
    }

    void init(AppConfig appConfig) {
        try {
            def xml = new XmlParser().parse("https://www10.ava.es/infotrafico/rlt/xml/dataMap1.xml".toURL().newInputStream())
            xml.CAM.eachWithIndex { item, idx ->
                Camara c = new Camara(ciudad: 'valladolid',
                        id: idx,
                        nombre: item["@uname"],
                        url: "https://www10.ava.es/infotrafico/${item["@img"]}"
                )
                camaras.add c
            }
        }catch(e){
            e.printStackTrace()
        }
    }
}
