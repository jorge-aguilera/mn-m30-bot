package mn.camaras.services

import groovy.transform.CompileStatic
import groovy.transform.TypeCheckingMode
import groovy.util.logging.Log
import groovy.xml.Namespace
import io.micronaut.core.io.ResourceResolver
import io.micronaut.core.io.scan.ClassPathResourceLoader
import mn.AppConfig
import mn.camaras.Camara

@CompileStatic(TypeCheckingMode.SKIP)
@Log
class BarcelonaService extends SearchCamaraService{

    @Override
    String getCiudad() {
        'barcelona'
    }

    Namespace gml = new Namespace("http://www.opengis.net/gml", 'gml')
    Namespace cite = new Namespace("http://www.opengeospatial.net/cite", 'cite')

    Node kml

    void init(AppConfig appConfig) {
        ClassPathResourceLoader loader = new ResourceResolver().getLoader(ClassPathResourceLoader.class).get()
        String xml = new InputStreamReader(loader.getResource(appConfig.barcelonaUrl).get().openStream(),'UTF-8').text
        kml = new XmlParser().parseText(xml)

        kml[gml.featureMember].eachWithIndex{ f, idx->

            String nombre = f[cite.cameres][cite.carretera].text()+" "+f[cite.cameres][cite.municipi].text()

            if( f[cite.cameres][cite.pk].text() )
                nombre +=" PK "+f[cite.cameres][cite.pk].text()

            Camara c = new Camara(ciudad: 'barcelona',
                    id: idx,
                    nombre: nombre,
                    url: f[cite.cameres][cite.link].text()
            )
            camaras.add c
        }
    }

}
